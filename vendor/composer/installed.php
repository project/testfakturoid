<?php return array(
    'root' => array(
        'name' => 'drupal/fakturoid_api',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '689557b6e720f9abb0be7f5bec1a56477047db40',
        'type' => 'drupal-module',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'drupal/fakturoid_api' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '689557b6e720f9abb0be7f5bec1a56477047db40',
            'type' => 'drupal-module',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'fakturoid/fakturoid-php' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'reference' => '2b4af8d21d95e1f4722c4dcb65392a2faceea3b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fakturoid/fakturoid-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
