<?php

namespace Drupal\fakturoid_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Fakturoid API routes.
 */
class FakturoidApiController extends ControllerBase {

  /**
   * Handles the webhook request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to the webhook request.
   */
  public function handleWebhook(Request $request) {
    // Implement your webhook handling logic here.
    // Access the request data using $request->getContent() or other methods.

    // Example: Log the webhook payload.
    \Drupal::logger('subscription_service')->notice($request->getContent());

    // Example: Return a response.
    return new Response('Webhook handled successfully.', 200);
  }
  /**
   * Builds the response.
   */
  public function build() {
    $client = new facturoid_client;

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
