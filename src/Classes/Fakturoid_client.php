<?php

namespace Drupal\fakturoid_api\Classes;

use Fakturoid\Client;

/**
 * Provides Fakturoid API Clients service.
 */
class fakturoid_client {

  /**
   * The Fakturoid client.
   *
   * @var \Fakturoid\Client
   */
  protected $client;

  public $slug = "adamschneider";
  public $api_email = "adam.schneiderr@gmail.com";

  public $api_key = "4dd27e12a233f8fda202cc8310438a2c1afb4f55";


  /**
   * Constructs a new Fakturoid_client object.
   */


  public function __construct() {
    require_once(getcwd().'/modules/custom/fakturoid_api/vendor/fakturoid/fakturoid-php/lib/Fakturoid/Client.php');
    $this->client = new Client($this->slug, $this->api_email, $this->api_key, 'PHPlib');
  }

  /**x
   * Returns the Fakturoid client.
   *
   * @return \Fakturoid\Client
   *   The Fakturoid client.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Creates a new client in Fakturoid.
   *
   * @param array $data
   *   The client data.
   *
   * @return mixed
   *   The created client.
   */
  public function createClient(array $data) {
    return $this->client->createSubject($data)->getBody();
  }
}
