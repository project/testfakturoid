<?php

namespace Drupal\fakturoid_api\Classes;

use Drupal\fakturoid_api\Classes\Fakturoid_client;

/**
 * Provides Fakturoid API Invoices service.
 */
class Fakturoid_invoice {

  /**
   * The Fakturoid client.
   *
   * @var \Fakturoid\Client
   */
  protected $client;

  /**
   * Constructs a new Fakturoid_invoice object.
   */
  public function __construct() {
    $this->client = new fakturoid_client();
  }

  /**
   * Returns the Fakturoid client.
   *
   * @return \Fakturoid\Client
   *   The Fakturoid client.
   */
  public function getClient() {
    return $this->client;
  }

  /**
   * Creates a new invoice in Fakturoid.
   *
   * @param array $data
   *   The invoice data.
   *
   * @return mixed
   *   The created invoice.
   */
  public function createInvoice(array $lines) {
    return $this->client->createInvoice($lines)->getBody();
  }

  /**
   * Fires an action on the specified invoice in Fakturoid.
   *
   * @param int $invoiceId
   *   The ID of the invoice.
   * @param string $action
   *   The action to perform ('deliver', 'pay', 'pay_proforma', 'pay_partial_proforma').
   *
   * @return mixed
   *   The updated invoice.
   */
  public function fireInvoiceAction($invoiceId, $action) {
    return $this->client->fireInvoice($invoiceId, $action)->getBody();
  }

  /**
   * Retrieves an invoice from Fakturoid.
   *
   * @param int $invoiceId
   *   The ID of the invoice.
   *
   * @return mixed
   *   The retrieved invoice.
   */
  public function getInvoice($invoiceId) {
    return $this->client->getInvoice($invoiceId)->getBody();
  }

  public function downloadInvoicePdf($invoiceId) {
    $i = 0;
    while (true) {
      $response = $this->client->getInvoicePdf($invoiceId);

      if ($response->getStatusCode() == 200) {
        $data = $response->getBody();
        break;
      }
      elseif ($i < 5) {
        return false;
      }
      $i++;
      sleep(1);
    }
  return $data;
  }

  /**
   * Filters subjects based on the custom ID attribute.
   *
   * @param string $customId
   *   The custom ID to filter subjects.
   *
   * @return mixed
   *   The filtered subjects.
   */
  public function filterSubjectsByCustomId($customId) {
    $response = $this->client->getSubjects(['custom_id' => $customId]);
    return $response->getBody();
  }

  /**
   * Retrieves a subject from Fakturoid based on the custom ID attribute.
   *
   * @param string $customId
   *   The custom ID to filter subjects.
   *
   * @return mixed|null
   *   The retrieved subject, or null if not found.
   */
  public function getSubjectByCustomId($customId) {
    $subjects = $this->filterSubjectsByCustomId($customId);
    if (count($subjects) > 0) {
      return $subjects[0];
    }
    return null;
  }

}
